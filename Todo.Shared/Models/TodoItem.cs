﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Todo.Shared.Models
{
    public class TodoItem
    {
        public Guid Id { get; set; }

        [Required]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTimeOffset? DueAt { get; set; }

        public bool Completed { get; set; }

        public string UserId { get; set; }

    }
}