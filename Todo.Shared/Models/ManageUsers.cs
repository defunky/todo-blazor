﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Todo.Shared.Models
{
    public class ManageUsers
    {
        public IdentityUser[] Admins { get; set; }
        public IdentityUser[] Users { get; set; }
    }
}
