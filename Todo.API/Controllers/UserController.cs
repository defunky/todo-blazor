﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.API.Services;
using Todo.Shared.Models;

namespace Todo.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> m_logger;
        private readonly IJwtTokenService m_tokenService;
        private readonly UserManager<IdentityUser> m_userManager;
        private readonly RoleManager<IdentityRole> m_roleManager;

        public UserController(ILogger<UserController> logger, IJwtTokenService tokenService, 
            UserManager<IdentityUser> userManager, 
            RoleManager<IdentityRole> roleManager)
        {
            m_logger = logger;
            m_tokenService = tokenService;
            m_userManager = userManager;
            m_roleManager = roleManager;
        }

        public async Task<List<IdentityUser>> Get()
        {
            return await m_userManager.Users.ToListAsync();
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] User user)
        {
            if (!ModelState.IsValid) return BadRequest();

            var result = await m_userManager.CreateAsync(new IdentityUser
            {
                UserName = user.Email,
                Email = user.Email
            }, user.Password);
            return !result.Succeeded ? (IActionResult) BadRequest(result.Errors) : Ok();
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] User user)
        {
            if (!ModelState.IsValid) return BadRequest();

            IdentityUser result = await m_userManager.FindByNameAsync(user.Email);
            var correct = await m_userManager.CheckPasswordAsync(result, user.Password);

            if (!correct) return BadRequest();

            return Ok(new { token = m_tokenService.BuildToken(user.Email, await m_userManager.GetRolesAsync(result)) });
        }

        //[HttpPost]
        //public IActionResult GenerateToken([FromBody] User user)
        //{
        //    return Ok(new { token = m_tokenService.BuildToken(user.Email ) });
        //}

        [HttpGet]
        [Route("Seed")]
        public async Task<IActionResult> Seed()
        {
            var res = await m_roleManager.RoleExistsAsync("Administrator");

            if (!res)
            {
                await m_roleManager.CreateAsync(new IdentityRole("Administrator"));
            }

            var test = m_userManager.Users.SingleOrDefault(x => x.UserName == "admin@a");

            if (test != null) return Ok();

            test = new IdentityUser()
            {
                UserName = "admin@a",
                Email = "admin@a"
            };

            await m_userManager.CreateAsync(test, "admin");
            await m_userManager.AddToRoleAsync(test, "Administrator");

            return Ok();
        }

    }
}