﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Todo.Shared.Models;

namespace Todo.API.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("[controller]")]
    [ApiController]
    public class ManageUsersController : ControllerBase
    {
        private readonly UserManager<IdentityUser> m_userManager;
        private readonly RoleManager<IdentityRole> m_roleManager;

        public ManageUsersController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            m_userManager = userManager;
            m_roleManager = roleManager;
        }

        [HttpGet]
        public async Task<ManageUsers> Get()
        {
            var admins = (await m_userManager.GetUsersInRoleAsync("Administrator")).ToArray();
            var users = await m_userManager.Users.ToListAsync();

            foreach (var admin in admins)
            {
                users.RemoveAll(x => x.Id == admin.Id);
            }

            return new ManageUsers()
            {
                Admins = admins,
                Users = users.ToArray()
            };
        }

        [HttpPost]
        [Route("DeleteUser")]
        public async Task<IActionResult> DeleteUser([FromBody] string id)
        {
            var user = m_userManager.Users.FirstOrDefault(x => x.Id == id);

            var res = await m_userManager.DeleteAsync(user);

            if (!res.Succeeded) return BadRequest("Could not delete user");

            return Ok();
        }

        [HttpPost]
        [Route("AddAdmin")]
        public async Task<IActionResult> AddAdmin([FromBody] string id)
        {
            var user = m_userManager.Users.FirstOrDefault(x => x.Id == id);

            var res = await m_userManager.AddToRoleAsync(user, "Administrator");

            if (!res.Succeeded) return BadRequest("Could not add admin");

            return Ok();
        }

        [HttpPost]
        [Route("RemoveAdmin")]
        public async Task<IActionResult> RemoveAdmin([FromBody] string id)
        {
            var user = m_userManager.Users.FirstOrDefault(x => x.Id == id);

            var res = await m_userManager.RemoveFromRoleAsync(user, "Administrator");

            if (!res.Succeeded) return BadRequest("Could not remove admin role");

            return Ok();
        }
    }
}