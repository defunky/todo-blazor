﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Todo.API.Services;
using Todo.Shared.Models;

namespace Todo.API.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly ITodoService m_itemService;
        private readonly IJwtTokenService m_tokenService;
        private readonly UserManager<IdentityUser> m_userManager;

        public TodoController(ITodoService itemService, IJwtTokenService tokenService, UserManager<IdentityUser> userManager)
        {
            m_itemService = itemService;
            m_tokenService = tokenService;
            m_userManager = userManager;
        }

        [HttpGet]
        public async Task<TodoItem[]> Get()
        {
            var token = m_tokenService.GetHeaderToken(Request.Headers);
            var email = m_tokenService.GetEmail(token);
            var user = await m_userManager.FindByEmailAsync(email);

            if (user == null) return null;

            return await m_itemService.GetIncompleteItemsAsync(user);
        }

        [HttpGet]
        [Route("Seed")]
        public async Task<IActionResult> Seed()
        {
            var user = await m_userManager.FindByEmailAsync("admin@a");

            if (user == null) return null;

            var res = await m_itemService.AddItemAsync(new TodoItem(){ Title = "Testing", DueAt = DateTimeOffset.UtcNow}, user);

            if (!res) return BadRequest("Could not add item");

            return Ok();
        }

        [HttpPost]
        [Route("AddItem")]
        public async Task<IActionResult> AddItem([FromBody] TodoItem item)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var token = m_tokenService.GetHeaderToken(Request.Headers);
            var email = m_tokenService.GetEmail(token);
            var user = await m_userManager.FindByEmailAsync(email);

            var res = await m_itemService.AddItemAsync(item, user);

            if (!res) return BadRequest("Could not add item");

            return Ok();
        }

        [HttpPost]
        [Route("MarkDone")]
        public async Task<IActionResult> MarkDone([FromBody] Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var token = m_tokenService.GetHeaderToken(Request.Headers);
            var email = m_tokenService.GetEmail(token);
            var user = await m_userManager.FindByEmailAsync(email);

            var res = await m_itemService.MarkDoneAsync(id, user);

            if (!res)
                return BadRequest("Could not mark item as done");

            return Ok();
        }

    }
}