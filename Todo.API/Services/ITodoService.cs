﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;
using Todo.Shared.Models;

namespace Todo.API.Services
{
    public interface ITodoService
    {
        Task<TodoItem[]> GetIncompleteItemsAsync(IdentityUser user);
        Task<bool> AddItemAsync(TodoItem item, IdentityUser user);
        Task<bool> MarkDoneAsync(Guid id, IdentityUser user);
    }
}
