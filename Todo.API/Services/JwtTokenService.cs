﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace Todo.API.Services
{
    public class JwtTokenService : IJwtTokenService
    {
        private readonly IConfiguration m_config;

        public JwtTokenService(IConfiguration config)
        {
            m_config = config;
        }

        /// <summary>
        /// Generates a JWT Token which can be used to access Authorized API calls
        /// </summary>
        /// <param name="email">The email/user the token will belong too</param>
        /// <param name="roles">The roles attached to the user</param>
        /// <returns>A JWT token</returns>
        public string BuildToken(string email, IEnumerable<string> roles)
        {
            //The claims for the token
            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Email, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }


            //The key which will be used to sign the token
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(m_config["Jwt:Key"]));

            //Credentials which is encrypted
            var credent = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            //The token to send back
            var token = new JwtSecurityToken(m_config["Jwt:Issuer"],
                m_config["Jwt:Audience"],
                claims,
                null,
                null,
                credent);

            //Returns token in a encoded string format
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Retrieves a Email encoded within a valid JWT token.
        /// </summary>
        /// <param name="token">A valid JWT token</param>
        /// <returns>The email the token belongs too</returns>
        public string GetEmail(string token)
        {
            var key = Encoding.UTF8.GetBytes(m_config["Jwt:Key"]);
            var handler = new JwtSecurityTokenHandler();
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = true,
                ValidateAudience = true,
                RequireExpirationTime = false,
                ValidAudience = m_config["Jwt:Audience"],
                ValidIssuer = m_config["Jwt:Issuer"]
            };
            var claims = handler.ValidateToken(token, validations, out var tokenSecure);
            return claims.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email)?.Value;
        }

        /// <summary>
        /// Gets the token located in the Authorization header when a request is made
        /// </summary>
        /// <param name="headers">The headers in request made</param>
        /// <returns>The JWT token that was sent in the request</returns>
        public string GetHeaderToken(IHeaderDictionary headers)
        {
            StringValues values;
            var res = headers.TryGetValue("Authorization", out values);
            if (!res) return null;

            string[] split = values.FirstOrDefault().Split(" ");
            return split.Last();
        }
    }
}
