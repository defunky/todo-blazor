﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Todo.API.Services
{
    public interface IJwtTokenService
    {
        string BuildToken(string email, IEnumerable<string> roles);
        string GetEmail(string token);
        string GetHeaderToken(IHeaderDictionary headers);
    }
}
