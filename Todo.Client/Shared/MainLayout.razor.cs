﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;

namespace Todo.Client.Shared
{
    public class MainLayoutBase : LayoutComponentBase
    {
        [Inject] protected ApplicationState AppState { get; set; }

        protected async Task Logout()
        {
            await AppState.Logout().ConfigureAwait(false);
            ApplicationState.IsLoggedIn = false;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await AppState.SetLoggedState().ConfigureAwait(false);
            await AppState.SetEmail().ConfigureAwait(false);

            if (firstRender)
            {
                await InvokeAsync(StateHasChanged).ConfigureAwait(false);
            }
        }

    }
}
