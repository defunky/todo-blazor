﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace Todo.Client.Shared
{
    public class NavMenuBase : ComponentBase
    {
        [Inject] protected ApplicationState AppState { get; set; }

        public NavMenuBase()
        {
            //TODO: This is been registered multiple times resulting in multiple event calls.
            ApplicationState.LoggingStateChanged += ApplicationState_LoggingStateChanged;
        }

        protected async Task Logout()
        {
            await AppState.Logout().ConfigureAwait(false);
            ApplicationState.IsLoggedIn = false;
        }

        private async void ApplicationState_LoggingStateChanged(object sender, EventArgs e)
        {
            await InvokeAsync(StateHasChanged).ConfigureAwait(false);
        }

        bool collapseNavMenu = true;

        protected string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        protected void ToggleNavMenu()
        {

            collapseNavMenu = !collapseNavMenu;
        }
    }
}
