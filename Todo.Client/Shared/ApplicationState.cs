﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;

namespace Todo.Client.Shared
{
    public class ApplicationState : ComponentBase
    {
        private readonly ILocalStorageService m_localStorage;
        private static bool m_isLoggedIn;
        public static event EventHandler LoggingStateChanged;
        public ApplicationState(ILocalStorageService localStorage)
        {
            m_localStorage = localStorage;
        }

        public static bool IsLoggedIn
        {
            get => m_isLoggedIn;
            set
            {
                m_isLoggedIn = value;
                OnLoggingStateChanged();
            }
        }

        public static string Email { get; set; }
        public static HashSet<string> Roles { get; } = new HashSet<string>();

        public async Task Logout()
        {
           await m_localStorage.RemoveItemAsync("token").ConfigureAwait(false);
        }

        public async Task SetLoggedState()
        {
            IsLoggedIn = await m_localStorage.ContainKeyAsync("token").ConfigureAwait(false);
        }

        public async Task SetEmail()
        {
            if (await m_localStorage.ContainKeyAsync("token").ConfigureAwait(false))
            {
                var handler = new JwtSecurityTokenHandler();
                var token = handler.ReadJwtToken(await m_localStorage.GetItemAsync<string>("token").ConfigureAwait(false));
                foreach (var claim in token.Claims)
                {
                    switch (claim.Type)
                    {
                        case JwtRegisteredClaimNames.Email:
                            Email = claim.Value;
                            break;
                        case ClaimTypes.Role:
                            Roles.Add(claim.Value);
                            break;
                    }
                }
                return;
            }

            Email = "";
        }

        private static void OnLoggingStateChanged()
        {
            LoggingStateChanged?.Invoke(null, EventArgs.Empty);
        }
    }
}
