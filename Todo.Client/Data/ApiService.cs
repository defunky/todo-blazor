﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Newtonsoft.Json;

namespace Todo.Client.Data
{
    public abstract class ApiService : IApiService
    {
        private readonly ILocalStorageService m_localStorage;

        protected ApiService(IHttpClientFactory factory, ILocalStorageService localStorage)
        {
            m_localStorage = localStorage; //Used to pull token for authentication
            ClientFactory = factory;
        }

        /// <summary>
        /// The client factory to create client typ
        /// </summary>
        public IHttpClientFactory ClientFactory { get; set; }

        /// <summary>
        /// Calls a GET Request on an API and gets content back as a string.
        /// </summary>
        /// <param name="url">The url to the API</param>
        /// <param name="validate">Apply a token bearer in header found in local storage</param>
        /// <returns>If it's successful and the content as string</returns>
        public async Task<(bool Success, string Content)> ExecuteGetAsync(Uri url, bool validate = true)
        {
            using var client = ClientFactory.CreateClient();

            if (validate)
            {
                var token = await m_localStorage.GetItemAsync<string>("token").ConfigureAwait(false);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var response = await client.GetAsync(url).ConfigureAwait(false);

            return (response.IsSuccessStatusCode, await response.Content.ReadAsStringAsync().ConfigureAwait(false));
        }

        /// <summary>
        /// Calls a GET Request on an API and gets content back as a deserialized type.
        /// </summary>
        /// <param name="url">The url to the API</param>
        /// <param name="validate">Apply a token bearer in header found in local storage</param>
        /// <returns>If it's successful and the content as specified Type</returns>
        public async Task<(bool Success, T Content)> ExecuteGetAsync<T>(Uri url, bool validate = true)
        {
            var response = await ExecuteGetAsync(url, validate).ConfigureAwait(false);

            return (response.Success, JsonConvert.DeserializeObject<T>(response.Content));
        }

        /// <summary>
        /// Calls a POST Request on an API and gets content back as a string.
        /// Note: Post Content must be in valid JSON format
        /// </summary>
        /// <param name="url">The url to the API</param>
        /// <param name="content">The post content in JSON format</param>
        /// <param name="validate">Apply a token bearer in header found in local storage</param>
        /// <returns>If it's successful and the content as string</returns>
        public async Task<(bool Success, string Content)> ExecutePostAsync(Uri url, string content, bool validate = true)
        {
            using var client = ClientFactory.CreateClient();
            using var postContent = new StringContent(content, Encoding.UTF8, "application/json");

            if (validate)
            {
                var token = await m_localStorage.GetItemAsync<string>("token").ConfigureAwait(false);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = await client.PostAsync(url, postContent).ConfigureAwait(false);

            return (response.IsSuccessStatusCode, await response.Content.ReadAsStringAsync().ConfigureAwait(false));
        }

        /// <summary>
        /// Calls a POST Request on an API and gets content back as a deserialized type.
        /// Note: Post Content must be in valid JSON format
        /// </summary>
        /// <param name="url">The url to the API</param>
        /// <param name="content">The post content in JSON format</param>
        /// <param name="validate">Apply a token bearer in header found in local storage</param>
        /// <returns>If it's successful and the content as specified Type</returns>
        public async Task<(bool Success, T Content)> ExecutePostAsync<T>(Uri url, string content, bool validate = true)
        {
            var response = await ExecutePostAsync(url, content, validate).ConfigureAwait(false);

            return (response.Success, JsonConvert.DeserializeObject<T>(response.Content));
        }

        
    }
}
