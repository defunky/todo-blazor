﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using Todo.Shared.Models;

namespace Todo.Client.Data
{
    public class AccountService : ApiService, IAccountService
    {
        private string m_url = "http://localhost:57604";
        public AccountService(IHttpClientFactory factory, ILocalStorageService localStorage) : base(factory, localStorage)
        {
        }

        public async Task<(bool Success, string Content)> Register(User user)
        {
            return await ExecutePostAsync(new Uri(m_url + "/user/register"), JsonConvert.SerializeObject(user), false).ConfigureAwait(false);
        }

        public async Task<(bool Success, string Content)> Login(User user)
        {
            return await ExecutePostAsync(new Uri(m_url + "/user/login"), JsonConvert.SerializeObject(user), false).ConfigureAwait(false);
        }
    }
}
