﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using Todo.Client.Data.Interfaces;
using Todo.Shared.Models;

namespace Todo.Client.Data
{
    public class ManageService : ApiService, IManageService
    {
        private string m_url = "http://localhost:57604";

        public ManageService(IHttpClientFactory factory, ILocalStorageService localStorage) : base(factory, localStorage)
        {
        }
        public async Task<(bool Success, ManageUsers Content)> GetUsers()
        {
            return await ExecuteGetAsync<ManageUsers>(new Uri($"{m_url}/manageusers")).ConfigureAwait(false);
        }

        public async Task<(bool Success, string Content)> DeleteUser(string Id)
        {
            return await ExecutePostAsync(new Uri($"{m_url}/manageusers/DeleteUser"), JsonConvert.SerializeObject(Id)).ConfigureAwait(false);
        }

        public async Task<(bool Success, string Content)> AddAdmin(string Id)
        {
            return await ExecutePostAsync(new Uri($"{m_url}/manageusers/addadmin"), JsonConvert.SerializeObject(Id)).ConfigureAwait(false);

        }

        public async Task<(bool Success, string Content)> RemoveAdmin(string Id)
        {
            return await ExecutePostAsync(new Uri($"{m_url}/manageusers/removeadmin"), JsonConvert.SerializeObject(Id)).ConfigureAwait(false);
        }
    }
}
