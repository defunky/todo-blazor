﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Shared.Models;

namespace Todo.Client.Data.Interfaces
{
    public interface ITodoService : IApiService
    {
        Task<(bool Success, IList<TodoItem> Content)> GetTodoList();
        Task<(bool Success, string Content)> MarkDone(Guid itemId);
        Task<(bool Success, string Content)> AddNewItem(TodoItem item);
    }
}
