﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Shared.Models;

namespace Todo.Client.Data.Interfaces
{
    public interface IManageService : IApiService
    {
        Task<(bool Success, ManageUsers Content)> GetUsers();
        Task<(bool Success, string Content)> DeleteUser(string Id);
        Task<(bool Success, string Content)> AddAdmin(string Id);
        Task<(bool Success, string Content)> RemoveAdmin(string Id);
    }
}
