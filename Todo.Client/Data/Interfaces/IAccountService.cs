﻿using System.Net.Http;
using System.Threading.Tasks;
using Todo.Shared.Models;

namespace Todo.Client.Data
{
    public interface IAccountService : IApiService
    {
        Task<(bool Success, string Content)> Register(User user);
        Task<(bool Success, string Content)> Login(User user);
    }
}