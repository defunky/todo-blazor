﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Todo.Client.Data
{
    public interface IApiService
    {
        IHttpClientFactory ClientFactory { get; set; }
        Task<(bool Success, string Content)> ExecuteGetAsync(Uri url, bool validate = true);
        Task<(bool Success, T Content)> ExecuteGetAsync<T>(Uri url, bool validate = true);
        Task<(bool Success, string Content)> ExecutePostAsync(Uri url, string content, bool validate = true);
        Task<(bool Success, T Content)> ExecutePostAsync<T>(Uri url, string content, bool validate = true);
    }
}