﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using Todo.Client.Data.Interfaces;
using Todo.Shared.Models;

namespace Todo.Client.Data
{
    public class TodoService : ApiService, ITodoService
    {
        private string m_url = "http://localhost:57604";

        public TodoService(IHttpClientFactory factory, ILocalStorageService localStorage) : base(factory, localStorage)
        {
        }

        public async Task<(bool Success, IList<TodoItem> Content)> GetTodoList()
        {
            return await ExecuteGetAsync<IList<TodoItem>>(new Uri($"{m_url}/todo")).ConfigureAwait(false);
        }

        public async Task<(bool Success, string Content)> MarkDone(Guid itemId)
        {
            return await ExecutePostAsync(new Uri($"{m_url}/todo/markdone"), JsonConvert.SerializeObject(itemId)).ConfigureAwait(false);
        }

        public async Task<(bool Success, string Content)> AddNewItem(TodoItem item)
        {
            return await ExecutePostAsync(new Uri($"{m_url}/todo/AddItem"), JsonConvert.SerializeObject(item)).ConfigureAwait(false);
        }
    }
}
