﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Todo.Client.Data;
using Todo.Shared.Models;

namespace Todo.Client.Pages
{
    public class RegisterBase : ComponentBase
    {
        [Inject] private IAccountService AccountService { get; set; }
        [Inject] private ILocalStorageService StorageService { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }

        protected string Email { get; set; }
        protected string Pass { get; set; }
        protected bool Failed { get; set; }
        protected string Error { get; set; }
        protected async Task Register()
        {
            var user = new User
            {
                Email = Email,
                Password = Pass
            };

            var register = await AccountService.Register(user).ConfigureAwait(false);
            if (register.Success)
            {
                //Registration was successful, lets log the user in automatically.
                var login = await AccountService.Login(user).ConfigureAwait(false);
                var token = (string)JsonConvert.DeserializeObject<JObject>(login.Content)["token"];
                await StorageService.SetItemAsync("token", token).ConfigureAwait(false);
                NavigationManager.NavigateTo("/", true);
            }
            else
            {
                Failed = true;
                try
                {
                    Error = JToken.Parse(register.Content).First["description"].ToString();
                }
                catch (InvalidOperationException) //Any errors we don't expect i.e empty email/pass etc...
                {
                    Error = "Registration has failed, please try another username/email.";
                }
            }
        }
    }
}
