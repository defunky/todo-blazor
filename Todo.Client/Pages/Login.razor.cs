﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Todo.Client.Data;
using Todo.Shared.Models;

namespace Todo.Client.Pages
{
    public class LoginBase : ComponentBase
    {

        [Inject] private IAccountService AccountService { get; set; }

        [Inject] private ILocalStorageService StorageService { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }

        protected string Email { get; set; }
        protected string Pass { get; set; }
        protected bool Failed { get; set; }

        protected async Task Login()
        {
            var user = new User
            {
                Email = Email,
                Password = Pass
            };

            var login = await AccountService.Login(user).ConfigureAwait(false);

            if (login.Success)
            {
                var token = (string)JsonConvert.DeserializeObject<JObject>(login.Content)["token"];
                await StorageService.SetItemAsync("token", token).ConfigureAwait(false);
                NavigationManager.NavigateTo("/", true);
            }
            else
            {
                Failed = true;
            }
        }
    }
}
