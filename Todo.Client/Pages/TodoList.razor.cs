﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Todo.Client.Data.Interfaces;
using Todo.Shared.Models;

namespace Todo.Client.Pages
{
    public class TodoBase : ComponentBase
    {
        [Inject] private ILocalStorageService StorageService { get; set; }
        [Inject] private ITodoService TodoService { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }

        protected IList<TodoItem> Items { get; set; } = new List<TodoItem>();

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                var token = await StorageService.GetItemAsync<string>("token").ConfigureAwait(false);
                if (token == null)
                {
                    NavigationManager.NavigateTo("/login", true);
                }
                else
                {
                    await refreshTodoList().ConfigureAwait(false);
                }
            }
        }

        private async Task refreshTodoList()
        {
            var todoList = await TodoService.GetTodoList().ConfigureAwait(false);
            if (todoList.Success)
            {
                Items = todoList.Content;
                await InvokeAsync(StateHasChanged).ConfigureAwait(false);
            }
        }

        protected async Task MarkDone(Guid itemId)
        {
            await TodoService.MarkDone(itemId).ConfigureAwait(false);
        }

        #region Add New Item
        public string Title { get; set; }
        public DateTime DueAt { get; set; }

        protected async Task AddNewItem()
        {
            var item = new TodoItem { Title = this.Title, DueAt = new DateTimeOffset(this.DueAt) };
            await TodoService.AddNewItem(item).ConfigureAwait(false);

            await refreshTodoList().ConfigureAwait(false);
        }

        #endregion

    }
}
