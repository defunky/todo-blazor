﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Todo.Client.Data;
using Todo.Client.Data.Interfaces;
using Todo.Client.Shared;

namespace Todo.Client.Pages
{
    public class ManageUsersBase : ComponentBase
    {
        [Inject] private NavigationManager NavigationManager { get; set; }
        [Inject] private IManageService ManageService { get; set; }

        protected Todo.Shared.Models.ManageUsers Users { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            //If you ain't admin it should auto-send you back.
            if (!ApplicationState.Roles.Contains("Administrator"))
            {
                NavigationManager.NavigateTo("/", true);
            }

            if (firstRender)
            {
                await getUsers().ConfigureAwait(false);
            }
        }

        private async Task getUsers()
        {
            var users = await ManageService.GetUsers().ConfigureAwait(false);
            if (users.Success)
            {
                Users = users.Content;
            }

            await InvokeAsync(StateHasChanged).ConfigureAwait(false);
        }

        protected async Task DeleteUser(string id)
        {
            var result = await ManageService.DeleteUser(id).ConfigureAwait(false);
            if (result.Success)
                await getUsers().ConfigureAwait(false);
        }

        protected async Task AddAdmin(string id)
        {
            var result = await ManageService.AddAdmin(id).ConfigureAwait(false);
            if (result.Success)
                await getUsers().ConfigureAwait(false);
        }

        protected async Task RemoveAdmin(string id)
        {
            var result = await ManageService.RemoveAdmin(id).ConfigureAwait(false);
            if (result.Success)
                await getUsers().ConfigureAwait(false);
        }
    }
}
